#!/usr/bin/python3
__author__ = "Bart Nijsse and Jasper Koehorst"
__copyright__ = "Copyright 2022, UNLOCK"
__credits__ = ["Bart Nijsse", "Jasper Koehorst"]
__license__ = "CC0"
__version__ = "1.0.0"
__status__ = "Development"

'''
Generates generic info table of the metagenomic assembly, mapping and binning statistics:

Inputs:
Identifier
Assembly fasta file
BAM file
Bin Contig mapping File, format: contig<tab>bin_name

Standard out:
ID
Reads
Assembly size
Contigs
n50
Largest contig
Mapped reads
Bins
Total bin size
Binned
Reads mapped to bins
'''

import sys
import argparse
import pysam

def parse_options():
  usage = "\nassembly_bins_readstats.py --bam bam_file --assembly assembly.fasta"
  description = "Generates generic info table of the metagenomic assembly, mapping and binning statistics"
  parser = argparse.ArgumentParser(usage=usage, description=description)

  input_group = parser.add_argument_group('Required arguments')
  input_group.add_argument("-b","--bam",       dest="bam_file", help="sorted BAM file", required=True, metavar="")
  input_group.add_argument("-a","--assembly",  dest="assembly", help="Assembly in fasta format", required=True, metavar="")
  input_group.add_argument("-c","--binContigs",dest="binContigs_file", help="binContigsFile, format: contig<tab>bin_name", required=True, metavar="")
  input_group.add_argument("-i","--identifier",dest="identifier", help="Identifier of the dataset", required=True, metavar="")

  inputs = parser.parse_args()
  return inputs

def assembly_stats(assembly_file_path):
  data_dict = {}
  # contig = ""
  total_size = 0
  contig_sizes = []
  contig_len = 0
  content = open(assembly_file_path,"r").readlines()
  for i,line in enumerate(content):
    line = line.strip()
    if line.startswith(">"):
      # contig = bin+"\t"+line.strip(">")
      if i != 0:
        contig_sizes.append(contig_len)
        contig_len = 0
    else:
      total_size += len(line.strip())
      contig_len += len(line.strip())  
  contig_sizes.append(contig_len)
  
  # N50
  size = []
  for contig_size in sorted(contig_sizes, reverse=True):
    size.append(contig_size)
    if sum(size) >= total_size * 0.5:
        n50 = contig_size
        break
  
  data_dict["assembly_size"] = str(total_size)
  data_dict["nr_contigs"] = str(len(contig_sizes))
  data_dict["largest_contig"] = str(max(contig_sizes))
  data_dict["assembly_n50"] = str(n50)

  return(data_dict)

def read_bin_stats(bam_file,binContigs_file):
  data_dict = {}
  # Get total reads from samtools flagstats
  total_reads = int(pysam.flagstat(bam_file).split("\n")[0].split()[0])

  # Get totals bins and all contigs with in bin from binContigsFile
  binContigs = set()
  bins = 0
  prev_bin = ""
  for line in open(binContigs_file).readlines():
    if not "unbinned" in line:
        if line.split()[1] != prev_bin: 
          bins+=1
        binContigs.add(line.strip().split()[0])
        prev_bin = line.split()[1]

  total_assembly_size = 0
  mapped_reads = 0

  total_bin_size = 0
  mapped_bin_reads = 0
  
  for line in pysam.idxstats(bam_file).split("\n"):
    if line:
      sline = line.split()
      contig = sline[0]
      contigLen = int(sline[1])
      contigReads = int(sline[2])

      total_assembly_size += contigLen
      mapped_reads += contigReads

      if contig in binContigs:
          total_bin_size += contigLen
          mapped_bin_reads += contigReads

  data_dict["total_reads"] = str(total_reads)
  data_dict["mapped_reads_perc"] = str(round(mapped_reads/total_reads*100,2))
  data_dict["bins"] = str(bins)
  data_dict["binned_contigs"] = str(len(binContigs))
  data_dict["total_bin_size"] = str(total_bin_size)
  data_dict["binned_perc"] = str(round(total_bin_size/total_assembly_size*100,2))
  data_dict["mapped_bin_reads_perc"] = str(round(mapped_bin_reads/total_reads*100,2))
  
  return data_dict


def main(argv):
  inputs = parse_options()
  stats = {}
  stats.update(assembly_stats(inputs.assembly))
  stats.update(read_bin_stats(inputs.bam_file, inputs.binContigs_file))

  print("ID"+"\t"+inputs.identifier)
  print("Reads"+"\t"+stats["total_reads"])
  print("Assembly size"+"\t"+stats["assembly_size"]+" bp")
  print("Contigs"+"\t"+stats["nr_contigs"])
  print("n50"+"\t"+stats["assembly_n50"]+" bp")
  print("Largest contig"+"\t"+stats["largest_contig"]+" bp")
  print("Mapped reads"+"\t"+stats["mapped_reads_perc"]+"%")
  print("Bins"+"\t"+stats["bins"])
  print("Binned"+"\t"+stats["binned_perc"]+"%")
  print("Binned contigs"+"\t"+stats["binned_contigs"])
  print("Total binned size"+"\t"+(stats["total_bin_size"])+" bp")
  print("Reads mapped to bins"+"\t"+stats["mapped_bin_reads_perc"]+"%")

if __name__ == "__main__":
	main(sys.argv[1:])