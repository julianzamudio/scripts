#!/bin/bash

# sh get_unbinned_contigs.sh <contigs.fasta> <folder containing .fa bins> <identifier>

cat $2/*.fa  | grep "^>" | sed 's/>//g' | sort > binned.headers
/unlock/infrastructure/binaries/misc/faSomeRecords $1 -exclude binned.headers $3_unbinned.fasta
pigz -p $4 $3_unbinned.fasta
