from irodsConnector import irodsConnector
from getpass import getpass
from irods.models import Collection, DataObject, Resource
from irods.column import Like

def main():
    envFile = 'irods_environment.json'
    password = '07A95239-F5C1-4625-8D29-D639E18A5C05' # getpass("irods password ...")
    ic = irodsConnector(envFile, password)

    collection = '/unlock/landingzone/projects/PRJ_UNLOCK/INV_CAMI/data/rhimg'
    coll = ic.session.collections.get(collection)

    metaKey = "RESOURCE"
    metaVal = "dual"

    #get physical object paths
    print("Retrieving physical data object paths...")
    objects = {}
    staging = {}
    for obj in coll.data_objects:
        #get physical path
        rescs = [r.resource_name for r in obj.replicas]
        #only get paths of data on archiveResc
        if len(rescs) == 1 and 'archiveResc' in rescs:
            objQuery = ic.session.query(DataObject.path)
            objQuery.filter(Like(DataObject.name, obj.name))
            objQuery.filter(Like(Collection.name, coll.path))
            path = [list(r.values())[0] for r in objQuery.get_results()][0]
            objects[obj.path] = path
        # break
    staging = objects.copy()
    exceptions = {}

    for object in objects:
        print(object)
        #get archive state (DUL --> workable, all other not workable)
        params = {'*dataPath': '"'+objects[object]+'"'}
        out = ic.executeRule('arcAttr.r', params)
        print("Test State:", out)
        print("Archive State:", out[0][0])
        if 'DUL' in out[0][0]:
            print("Moving to dual:", object)
            obj = ic.session.data_objects.get(object)
            ic.updateMetadata([obj], metaKey, metaVal)
            obj = ic.session.data_objects.get(object)
            print(obj.replicas)
            staging.pop(object)
            print()
        elif out[0][0] == '':
            print('No state information available')
            exceptions[object] = objects[object]
            staging.pop(object)
            print()
        else:
            out = ic.executeRule('arcGet.r', params)
            print("Object staging, check back later...")

    objects = staging.copy()
    while len(objects) > 0:
        for object in objects:
            params = {'*dataPath': '"'+objects[object]+'"'}
            out = ic.executeRule('arcAttr.r', params)
            print("Archive State:", out[0][0])
            if 'DUL' in out[0][0]:
                print("Moving to dual:", object)
                obj = ic.session.data_objects.get(object)
                ic.updateMetadata([obj], metaKey, metaVal)
                obj = ic.session.data_objects.get(object)
                print(obj.replicas)
                staging.pop(object)
                print()
            elif out[0][0] == '':
                print('No state information available')
                exceptions[object] = objects[object]
                staging.pop(object)
                print()
        objects = staging.copy()

    print("Data without state information  (Contact SURF!):")
    print(exceptions)

if __name__ == "__main__":
    main()