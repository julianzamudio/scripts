#!/bin/bash
#==========================================================================================
#title          :Unlock deploy
#description    :Deploy script by copying script files to the corresponding iRODS webdav folder
#author         :Bart Nijsse & Jasper Koehorst
#date           :2021
#version        :0.0.2
#==========================================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

###################################
# Mac volume mount location
DIRECTORY=/Volumes/unlock-icat.irods.surfsara.nl/infrastructure/scripts/

if [ -d "$DIRECTORY" ]; then
  echo "Copying files to "$DIRECTORY
  rsync -vrh --size-only $DIR/ $DIRECTORY/ --delete  --exclude ".git/*"
fi

###################################
# Other mount locations
DIRECTORY=/run/user/1000/gvfs/dav:host=unlock-icat.irods.surfsara.nl,ssl=true/infrastructure/scripts/

if [ -d "$DIRECTORY" ]; then
  echo "Copying files to "$DIRECTORY
  rsync -vrh --size-only $DIR/ $DIRECTORY/ --delete --exclude ".git/*"
fi

###################################
# Sync start for kubernetes nodes #
###################################

DIRECTORY=$DIR/../sync
if [ -d "$DIRECTORY" ]; then
  $DIR/../sync/sync.sh --infrastructure
fi
