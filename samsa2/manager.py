import sys, os

if "seed.daa.tsv" in sys.argv[1]:
    command = "python3 /unlock/infrastructure/binaries/scripts/samsa2/DIAMOND_subsystems_analysis_counter.py -I " + sys.argv[1] + " -O " + sys.argv[2] + "_SEED.hierarchy"
    print(command)
    os.system(command)
    command = "python3 /unlock/infrastructure/binaries/scripts/samsa2/subsys_reducer.py -I " + sys.argv[2] + "_SEED.hierarchy -O " + sys.argv[2] + "_SEED.reduced"
    print(command)
    os.system(command)

if "refseq.daa.tsv" in sys.argv[1]:
    command = "python3 /unlock/infrastructure/binaries/scripts/samsa2/standardized_DIAMOND_analysis_counter.py -I " + sys.argv[1] + " -F " + " -o " + sys.argv[2] + "_RefSeq"
    print(command)
    os.system(command)
    command = "python3 /unlock/infrastructure/binaries/scripts/samsa2/standardized_DIAMOND_analysis_counter.py -I " + sys.argv[1] + " -O -o " + sys.argv[2] + "_RefSeq"
    print(command)
    os.system(command)
