from irods.session import iRODSSession
from irods.column import Criterion
from irods.models import DataObject, DataObjectMeta, Collection, CollectionMeta
import ssl, os

def execute(command):
    os.system(command)

host = "unlock-icat.irods.surfsara.nl"
port = "1247"
zone = "unlock"

context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=None, capath=None, cadata=None)

ssl_settings = {'irods_client_server_negotiation': 'request_server_negotiation',
                'irods_client_server_policy': 'CS_NEG_REQUIRE',
                'irods_encryption_algorithm': 'AES-256-CBC',
                'irods_encryption_key_size': 32,
                'irods_encryption_num_hash_rounds': 16,
                'irods_encryption_salt_size': 8,
                'ssl_context': context}

session = iRODSSession(host = host,
                  port = port,
                  user = "Mr.Robot",
                  password = "0f57da8b-eb82-4092-9e02-7bab1e3b3acc",
                  zone = zone,
                  **ssl_settings)

print("Session", session)

results = session.query(DataObject.name).filter( \
        Criterion('like', Collection.name, '/unlock/references/genomes%GCA_%')).filter( \
        Criterion('like', DataObject.name, 'GCA%.yaml'))

todos = set()
for index, result in enumerate(results):
    data_name = result[DataObject.name]
    todos.add(data_name)


results = session.query(Collection.name, DataObject.name).filter( \
        Criterion('like', DataObject.name, '%.dat.gz')).filter( \
        Criterion('like', Collection.name, '/unlock/references%GCA_%')).filter( \
        Criterion('>', DataObject.size, '100'))

finished = {}

for result in results:
    finished[result[DataObject.name]] = result[Collection.name] + "/" + result[DataObject.name]

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

for todo in todos:
    print(todo)
    if todo + ".dat.gz" not in finished:
        yaml = session.query(Collection.name, DataObject.name).filter(
            Criterion('like', DataObject.name, '%.yaml')).filter(
                Criterion('like', Collection.name, '/unlock/references%' + todo + "%"))
        for item in yaml:
            item = item[Collection.name] + "/" + item[DataObject.name]
            command = "/run.sh -c /unlock/infrastructure/cwl/workflows/workflow_ena_retrieval.cwl -p false -y " + item
            execute(command)
            file1 = open(__location__ +"/processed.txt", "a")  # append mode
            file1.write(todo + "\n")
            file1.close()
    else:
        print("Already finished", todo)

print(session)
session.cleanup()