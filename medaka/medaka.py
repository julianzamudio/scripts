#!/venv/bin/python

# Improvement for the medaka workflow
import subprocess
import argparse
import gzip, os

# import multiprocessing
# from joblib import Parallel, delayed

def main():
	args = parse_arguments()
	mini_align(args)
	medaka_consensus(args)
	medaka_stich(args)

def parse_arguments():
	# Initialize parser
	parser = argparse.ArgumentParser()
	 
	# Adding optional argument
	parser.add_argument("-i", "--input", help = "input fast(a/q) file", required=True)
	parser.add_argument("-r", "--reference", help = "reference fasta file", required=True)
	parser.add_argument("-t", "--threads", help = "Number of threads to use", nargs='?', default=2, type=int)
	# parser.add_argument("-m", help = "fill MD tag")
	parser.add_argument("-p", help = "output file prefix (default: reads)",nargs='?', default="reads", type=str)
	parser.add_argument("-c", "--contig-chunks", help = "Number of contigs per medaka_consensus step", nargs='?',default=100, type=int)
	parser.add_argument("-s", "--size-chunks", help = "Maximum size in bases for a chunk", nargs='?',default=10000, type=int)
	parser.add_argument("--model", help = "Model definition, default is equivalent to r941_min_fast_g303.", default="r941_min_fast_g303")
	# Adding optional argument for ....
	# ...

	# Adding optional argument for ....
	# ...

	# Read arguments from command line
	args = parser.parse_args()
	return args
	


## SINGLE RUN
# # align reads to assembly
# mini_align -i basecalls.fasta -r assembly.fasta -P -m \
#     -p calls_to_draft.bam -t <threads>

def mini_align(args):
	# Checks for mmi and bam file?
	if os.path.exists(args.reference + ".mmi") and os.path.exists(args.p + ".bam"):
		print("Index and bam file are already present")
		return
	print(args)
	arguments = ["mini_align", "-i", args.input, "-r", args.reference, "-m", "-p", args.p, "-t", args.threads]
	command = ' '.join(map(str,arguments))
	print(">", command)
	subprocess.run(command, shell=True, check=True)


## RUN IN PARALLEL
# getconf ARG_MAX # To obtain maximum length

# # run lots of jobs like this, change model as appropriate
# mkdir results
# medaka consensus calls_to_draft.bam results/contigs1-4.hdf \
#     --model r941_min_fast_g303 --batch 200 --threads 8 \
#     --region contig1 contig2 contig3 contig4
# ...

def medaka_consensus(args):
	if not os.path.exists("results"):
		os.mkdir("results")

	chunks = {}
	# Read genome file to obtain contig information
	if (args.reference.endswith(".gz")):
		reader = gzip.open(args.reference, 'rb')
	else:
		reader = open(args.reference)

	# Chunk based on number of chunks and genome size
	size = 0
	chunk = 0
	
	for line in reader:
		if type(line) != str:
			line = line.decode('ascii')
		line = line.strip()
		if line.startswith(">"):
			header = line.strip(">").strip()			
			if chunk not in chunks:
				chunks[chunk] = set()
			if len(chunks[chunk]) > args.contig_chunks or size > args.size_chunks:
				size = 0
				chunk = chunk + 1
				chunks[chunk] = set()
			chunks[chunk].add(header)
		else:
			size = size + len(line)
	reader.close()

	# Run this in parallel for each chunk by number of threads / 2
	commands = []
	for chunk in chunks:
		chunk_path = "results/chunk_" + str(chunk) + ".hdf"
		if os.path.isfile(chunk_path):
			print("Chunk already exists in",chunk_path)
		else:
			# Quiet due to the parallel nature and the many processes
			arguments = ["medaka", "consensus", "--quiet", args.p + ".bam", chunk_path, "--model", args.model, "--threads", "2", "--region", ' '.join(chunks[chunk])]
			arguments = ' '.join(map(str,arguments))
			commands.append(arguments)
	
	# Multi thread section
	from functools import partial
	from multiprocessing.dummy import Pool
	from subprocess import call

	jobs = int(args.threads / 2)
	print("Number of jobs to be executed in parallel:", len(commands), "with maximum of", jobs, "jobs")
	pool = Pool(jobs) # two concurrent commands at a time
	for i, returncode in enumerate(pool.imap(partial(call, shell=True), commands)):
		if returncode != 0:
			print("%d command failed with exit code %d and command %s" % (i, returncode, commands[i]))
		else:
			print("%d command finished: %d %s" % (i, returncode, commands[i]))


## SINGLE RUN
# # wait for jobs, then collate results
# medaka stitch results/*.hdf polished.assembly.fasta
def medaka_stich(args):
	arguments = ["medaka", "stitch", "--quiet", "--threads", str(args.threads), "results/*.hdf", args.reference, "polished_" + os.path.basename(args.reference)]
	print(arguments)
	command = ' '.join(map(str,arguments))
	print(">", command)
	subprocess.run(command, shell=True, check=True)


if __name__ == '__main__':
	main()
